<?php
$user = isset($_REQUEST['user']) ? trim($_REQUEST['user']) : "";
if (empty($user)) {
?>
Hello, please tell us who you are!<br />
<form method="GET"><input type="text" name="user" placeholder="Your name" /><input type="submit" value="Send" /></form>
<?php
} else {
?>
Howdy, <?php=htmlspecialchars($user, ENT_COMPAT | ENT_HTML401, 'UTF-8', true)?>!
<?php
}

$html = <<<HTML;
<br /><b>This is some html {$user}</b>
HTML;

echo $html;
?>